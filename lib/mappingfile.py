#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import os
import re

class Mappingfile:
    
    '''
    - sucht gemäss "to" nach entsprechendem file for append mail body
    - in mapfile only lowercase. TO could be still upper!
    - not found email adress will be added with default dir/filename
    
    '''

    def __init__(self, app):
        self.opt = app.opt
        self.cfg = app.cfg
        self.mapfile = {}
        
    def delMapFile(self):
        '''
        del map file and create empty one
        '''
        filename = self.opt['--mapfile']
        cupw("del map file=", filename)
        if os.path.exists(filename):
            os.remove(filename)
            
        with open(self.opt['--mapfile'], 'w') as fh:
            pass
            
        self.loadMapFile()
        
    
    def loadMapFile(self):
        self.mapfile = {}
        with open(self.opt['--mapfile'], 'r') as fh:
            data = fh.read()
            
        for line in data.split(self.cfg['mapper']['nl']):
            if line != "":
            # ~ if line.find(self.cfg['delimiter']): #take only lines contain ,
                l1 = line.split(self.cfg['mapper']['delimiter'])
                if l1[0].find(self.cfg['mapper']['klammeraff']): #take only contain @
                    # ~ if l1[1] != self.cfg['noentry']: #take everything except ???
                    self.mapfile[l1[0]] = l1[1]
            
    
    
    def removeSpezChar(self, str1):
        return re.sub('[^a-zA-Z0-9]', '', str1)


    def getExportDirFilename(self, to): #<str1@str2 >path/filename.suffix
        to = to.lower() #use only lowercase
        toFormated = self.splitTo(to)
        filename = self.opt['--exportdir']
        filename += self.opt['--export'] + "/" + toFormated 
        filename += "." + self.opt['--export']  #default filename
        
        if toFormated in self.mapfile:
            filename = self.mapfile[toFormated]
        
        else:
            self.addToMapfile(toFormated, filename)
            
        return filename

        
    def addToMapfile(self, to, filename):
        str1 = to + self.cfg['mapper']['delimiter'] + filename + self.cfg['mapper']['nl']
        with open(self.opt['--mapfile'], "a") as fh:
            fh.write(str1)
            
        cupw("added to mapfile=", str1.rstrip())
        self.loadMapFile() # reload self.mapfile
    
    
    def splitTo(self, to):
        l1 = to.split(self.cfg['mapper']['klammeraff'])
        l1[0] = self.removeSpezChar(l1[0])
        l1[1] = self.removeSpezChar(l1[1])
        return l1[0] + self.cfg['mapper']['sepfilename'] + l1[1]
        
        
        

        
        
        
        
        

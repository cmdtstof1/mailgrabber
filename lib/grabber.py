#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import lib.mailserver
import lib.mappingfile
import lib.writer.txt_new
import lib.maily


class Grabber:
    
    '''
    grabbes the draft email
    - add nws in news file
    - save email
    - del email 
    '''

    def __init__(self, app):
        self.app = app
        self.opt = app.opt
        self.cfg = app.cfg
        
        
    def main(self):
        mf = lib.mappingfile.Mappingfile(self.app)
        mf.loadMapFile()
        ms = lib.mailserver.Mailserver(self.app)
        ms.connect()
        nums = ms.getDraftsNums()
        cupw("nums=", nums)
        m = lib.maily.Maily(self.app)
        
        for num in nums:
            cupw("###################################################################")
            cupw("processing mail num=", num)
            typ, data = ms.getMailNum(num, '(RFC822)')
            # ~ cupw("data=", data)
            maildict = m.setMail(data)
            if m.checkGrabberMail():
                exportFilename = mf.getExportDirFilename(maildict['to'])
                
                if self.opt['--export'] == "txt":
                    wr = lib.writer.txt_new.Exporter(self.app)
                    wr.add(exportFilename, maildict)
                    cupw("mail written into newsfile=", exportFilename)
                    
                else:
                    cupw("not supported exporter=", exporter)
                    exit()
                    
                #save mail to archive #TODO txt exporter verwenden???
                archivFilename = m.saveMail()
                cupw("mail saved to=", archivFilename)
                
                #deleted mail on server
                if int(self.opt['--maildel']):
                    ms.delMail(num)
                    cupw("mail deleted=", num)
                    
            else:
                cupw("******** NOT processing=", num)
                
                
        ms.closer()


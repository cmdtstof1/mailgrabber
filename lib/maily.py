#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import datetime
import email
import re

class Maily:

    def __init__(self, app):
        self.opt = app.opt
        self.cfg = app.cfg['mail']
        #self.mail = # email.message_from_string(raw_email_string)
        #self.maildict['from'] = #
        #self.maildict['to'] = #
        #self.maildict['local_date'] = # datetime object
        #self.maildict['local_message_date'] = #Wed, 10 Feb 2021 07:50:59
        #self.maildict['date'] = str(self.maildict'local_date') #2021-02-10 08:53:18
        #self.maildict['subject'] = subject
        #self.maildict['body'] = body
        self.spliter = "\n" #TODO maybe in cfg???
        
        
    def setMail(self, mail): #<mail data > maildict
        raw_email = mail[0][1]
        raw_email_string = raw_email.decode('utf-8')
        self.mail = email.message_from_string(raw_email_string)
        cupw("self.mail=", self.mail)
        
        self.maildict = {}
        
        # Header Details
        self.maildict['date'] = "empty date" #TODO from cfg
        date_tuple = email.utils.parsedate_tz(self.mail['Date'])
        if date_tuple:
            self.maildict['local_date'] = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            self.maildict['local_message_date'] = "%s" %(str(self.maildict['local_date'].strftime("%a, %d %b %Y %H:%M:%S")))
            self.maildict['date'] = str(self.maildict['local_date'])
        
        if 'From' in self.mail:
            self.maildict['from'] = str(email.header.make_header(email.header.decode_header(self.mail['From'])))
        else:
            self.maildict['from'] = "" #TODO from cfg
        
        if 'To' in self.mail:
            self.maildict['to'] = str(email.header.make_header(email.header.decode_header(self.mail['To'])))
        else:
            self.maildict['to'] = "" #TODO from cfg
            
        if 'Subject' in self.mail:    
            self.maildict['subject'] = str(email.header.make_header(email.header.decode_header(self.mail['Subject'])))
        else:
            self.maildict['subject'] = "" #TODO from cfg
        
        # Body details
        self.maildict['body'] = ""
        cupw("get_content_type=", self.mail.get_content_type(), "; get_charsets=", self.mail.get_charsets(), "; get_content_charset=", self.mail.get_content_charset(), "; is_multipart=", self.mail.is_multipart())
        
        if self.mail.get_content_type() == "text/plain":
            # ~ cupw("1) self.mail.get_payload()=", self.mail.get_payload())
            # ~ cupw("2) self.mail.get_payload(decode=True)=", self.mail.get_payload(decode=True))
            # ~ cupw("3) self.mail.get_payload(decode=True).decode('utf-8')=", self.mail.get_payload(decode=True).decode('utf-8'))
            
            # ~ if self.mail.get_content_charset() == "utf-8":
                # ~ cupw("get_content_charset() == utf-8")
                # ~ self.maildict['body'] = self.mail.get_payload(decode=True)
            # ~ else:
                # ~ cupw("get_content_charset() != utf-8")
            self.maildict['body'] = self.mail.get_payload(decode=True).decode('utf-8')
        else:
            #TODO not text/plain mail should also be processed
            cupw("!!!!!!! kein text/plain email !!!!!!")
            
        return self.maildict

        
    def checkGrabberMail(self): #TODO more checks. checks only for klammeraff until now!!!
        ok = 0
        to = 0
        body = 0
        if self.maildict['to'].find(self.cfg['klammeraff']) > 0:
            to = 1
            
        if self.maildict['body'] != "":
            body=1 
            
        if to and body:
            ok=1
        
        cupw("ok=", ok)
        return ok
    
    
    def getMailArchivDir(self):
        return self.opt['--mailarchiv']
    
    def getMailDict(self):
        return self.maildict
        
    def getTo(self):
        return self.maildict['to']
        
        
    def getArchivFilename(self): #>2021-02-10_085318_subject.txt
        filename = self.opt['--mailarchiv']
        filename += self.getFormatedDateStr() + "-" #2021-02-10 08:53:18
        filename += self.getFormatedSubjectStr()
        filename += ".txt"
        return filename
    
    def getFormatedSubjectStr(self):
        return re.sub('[^a-zA-Z0-9]', '', self.maildict['subject'])
    
    def getFormatedDateStr(self):
        return str(self.maildict['local_date'].strftime("%Y%m%d_%H%M%S"))
    
    
    def saveMail(self):
        #TODO use >>>>>>> mailren!!!!!! >>> email.parser
        str1 = self.maildict['date'] + self.spliter
        str1 += self.maildict['subject'] + self.spliter
        str1 += self.maildict['to']  + self.spliter
        str1 += self.maildict['body'] + self.spliter
        
        filename = self.getArchivFilename() 
        with open(filename, "w") as fh:
            fh.write(str1)
            
        return filename
        
        
    def setDictFromMailFile(self, filename):
        with open(filename, "r") as fh:
            data = fh.read()
            # ~ cupw(data)
            self.maildict = {}
            l2 = data.split(self.spliter, 3)
            # ~ cupw("len l2=", len(l2))
            # ~ if len(l2) > 4:
            self.maildict['date'] = l2[0]
            self.maildict['subject'] = l2[1]
            self.maildict['to'] = l2[2]
            self.maildict['body'] = l2[3]
            #TODO find url in body
        
        cupw("self.maildict=", self.maildict)
        return self.maildict
                    
            
        
        
    

#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import os
import lib.mappingfile
import lib.maily
import lib.writer.txt_new


class Newsbuilder:
    
    '''
    builds all news files out of the stored emails
    
    '''

    def __init__(self, app):
        self.app = app
        self.opt = app.opt
        self.cfg = app.cfg
        
        cupw("self.opt=", self.opt)
        cupw("self.cfg=", self.cfg)
        
        
    def buildNews(self):

        mf = lib.mappingfile.Mappingfile(self.app)
        m = lib.maily.Maily(self.app)
        
        #delete everything
        mf.delMapFile() ## del mapfile.csv
        
        ## del --exportdir'
        if self.opt['--export'] == "txt":
            wr = lib.writer.txt_new.Exporter(self.app)
            
        else:
            cupw("not supported exporter=", exporter)
            exit()

        wr.delAllNewsFiles() #del all news files
        
        dirMail = m.getMailArchivDir()
        cupw("process emails...=", dirMail)
        maillist = sorted(os.listdir(dirMail))#get email file list

        for f in maillist:
            cupw(f)
            filenameMail = dirMail + f
            maildict = m.setDictFromMailFile(filenameMail)
            filenameExport = mf.getExportDirFilename(maildict['to']) #check in mapping file
            wr.add(filenameExport, maildict)
            cupw("mail written into newsfile=", filenameExport)
        

#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import glob
import datetime

class Timeliner:
    
    '''
    makes lovely output out of the news (txt, html, )
        
    '''

    def __init__(self, app):
        self.opt = app.opt
        self.cfg = app.cfg

    def main(self):
        self.getNews()
        # ~ tl.prnNewsList()
        self.buildTimelineTxt()
        self.buildTimelineHtml()
        
        self.buildTimelineTxtTopic() #sort by topic
        self.buildTimelineHtmlTopic() #sort by topic
    
    
    
    def getNews(self):
        '''
        loads all news files
        creates sorted list of dicts
        '''
        cupw("self.opt", self.opt)
        cupw("self.cfg", self.cfg)
        
        #get filelist
        dirname = self.opt['--exportdir'] + self.opt['--export'] + "/"
        cupw("get filelist in=", dirname)
        filelist = glob.glob(dirname + "*") #get all

        # extract news into list of dict
        nlist = []
        for f in filelist:
            # ~ cupw("filename=", f)
            
            with open(f, "r") as fh:
                l1 = fh.read().split(self.cfg['export']['mailend']['txt'])
                # ~ cupw("l1=", l1)
                
                for txt in l1:
                    d1 = {}
                    l2 = txt.split(self.cfg['export']['nl']['txt'], 4)
                    # ~ cupw("len l2=", len(l2))
                    if len(l2) > 4:
                        d1['datum'] = l2[0]
                        d1['title'] = l2[1]
                        d1['topic'] = l2[2]
                        d1['url'] = l2[3]
                        d1['text'] = l2[4]
                        # ~ cupw("d1=", d1)
                        nlist.append(d1)
                        
        
        self.nlistsortdate = sorted(nlist, key=lambda k: k['datum'])
        seq = [d['datum'] for d in self.nlistsortdate]
        
        dateMin = min(seq)
        if len(dateMin) > 10:
            date1 = datetime.datetime.strptime(dateMin, '%Y-%m-%d %H:%M:%S')
        else:
            date1 = datetime.datetime.strptime(dateMin, '%Y-%m-%d')
            
        self.dateoldest = datetime.datetime(date1.year, date1.month, date1.day)
        
        dateMax = max(seq)
        if len(dateMax) > 10:
            date1 = datetime.datetime.strptime(dateMax, '%Y-%m-%d %H:%M:%S')
        else:
            date1 = datetime.datetime.strptime(dateMax, '%Y-%m-%d')
            
        self.datenewest = datetime.datetime(date1.year, date1.month, date1.day)
        cupw("self.dateoldest=", self.dateoldest, " self.datenewest=", self.datenewest)

        self.nlistsorttopic = sorted(nlist, key=lambda k: k['topic'].lower())
        seq = [d['topic'] for d in self.nlistsorttopic]
        self.topicfirst = min(seq)
        self.topiclast = max(seq)
        cupw("self.topicfirst=", self.topicfirst, " self.topiclast=", self.topiclast)
        
        
    def prnNewsList(self):
        '''
        print sorted news list (for test only)
        '''
        for n in self.nlistsortdate:
            cupw(n['datum'], n['title'])
        
        
    
    
    def buildTimelineTxt(self):
        '''
        build txt timeline file (sort by date)
        '''
        cupw("buildTimelineTxt...")
        str1 = ""
        for n in self.nlistsortdate:
            str1 += n['datum'] + " "
            str1 += self.formTopic(n['topic']) + " "
            str1 += n['title'][0:self.cfg['timeline']['titlelen']]
            str1 += self.cfg['timeline']['nl']['txt']
            
        filename = self.opt['--exporttimelinedir'] + self.opt['--exporttimelinefile'] + ".txt"
        cupw("write timeline to=", filename)
        with open(filename, "w") as text_file:
            text_file.write(str1)
        


    def buildTimelineTxtTopic(self):
        '''
        build txt timeline file (sort by topic)
        '''
        cupw("buildTimelineTxtTopic...")
        str1 = ""
        for n in self.nlistsorttopic:
            str1 += self.formTopic(n['topic']) + " "
            str1 += n['datum'] + " "
            str1 += n['title'][0:self.cfg['timeline']['titlelen']]
            str1 += self.cfg['timeline']['nl']['txt']
       
        filename = self.opt['--exporttimelinedir'] + self.opt['--exporttimelinefiletopic'] + ".txt"
        cupw("write timeline to=", filename)
        with open(filename, "w") as text_file:
            text_file.write(str1)
        


    def getTlHtmlTopicFilename(self): #sort by topic
        filename = self.opt['--exporttimelinedirhtml'] + self.opt['--exporttimelinefiletopic'] + ".html"
        # ~ cupw(filename)
        return filename


    def getTlHtmlTopicLink(self): #sort by topic
        filename = self.opt['--exporttimelinewwwbase'] + self.opt['--exporttimelinefiletopic'] + ".html"
        # ~ cupw(filename)
        return filename
        
        
    def getTlHtmlFilename(self): #sort by date
        filename = self.opt['--exporttimelinedirhtml'] + self.opt['--exporttimelinefile'] + ".html"
        # ~ cupw(filename)
        return filename


    def getTlHtmlLink(self): #sort by date
        filename = self.opt['--exporttimelinewwwbase'] + self.opt['--exporttimelinefile'] + ".html"
        # ~ cupw(filename)
        return filename
    
    
    def getHtmlHeader(self):
        str1 = '''
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css1.css" type="text/css" >
<title>
'''
        str1 += "stof999 newstimeliner"
        str1 += ''' 
</title>
</head>
<body>
'''
        return str1
    
    
    def getDateYYMMDDHHMM(self):
        return datetime.datetime.now().strftime("%Y%m%d %H:%M")
    
    def getNextNavDate(self, date1):
        dnew = date1
        if self.opt['--timelinenav'] == "month":
            if date1.month < 11:
                dnew = datetime.datetime(date1.year, date1.month + 1, 1)
            else:
                dnew = datetime.datetime(date1.year + 1, 1, 1)                
            
        else: #nav = day intervall
            dnew = date1 + datetime.timedelta(days=1)
        
        return dnew
        
                
    def getPrevNavDate(self, date1):
        dnew = date1
        if self.opt['--timelinenav'] == "month":
            if date1.month > 1:
                dnew = datetime.datetime(date1.year, date1.month - 1, 1)
            else:
                dnew = datetime.datetime(date1.year - 1, 12, 1)                
            
        else: #nav = day intervall
            dnew = date1 + datetime.timedelta(days=-1)
        
        return dnew
        # ~ return datetime.datetime(date1.year, date1.month - 1, 1)
        # ~ return datetime.datetime(date1.year, date1.month, date1.day -1)
        
    
    def buildTimelineHtml(self):
        '''
        builds html timeline file (sort by date)
        '''
        cupw("buildTimelineHtml...")
        str1 = self.getHtmlHeader()
        str1 += "<h1>stof999 newstimeliner (sort by date) " + self.getDateYYMMDDHHMM() + "</h1>"

        #date navigation header
        str1 += "<p class='navheader'>"
        str1 += "<a class='anchor' name='top' href='#top'>top</a> | " 
        str1 += "<a href='#bottom'>bottom</a>"
        str1 += "</p>"

        #table header row
        str1 += "<table>"
        str1 += "<tr>"
        str1 += "<th>date v</th>"
        str1 += "<th><a href='" + self.getTlHtmlTopicLink() + "'>topic</a></th>"
        str1 += "<th>title</th>"
        str1 += "</tr>"
        
        nextNavDate = self.getNextNavDate(self.dateoldest)
        
        for n in self.nlistsortdate:
            
            date1 = n['datum']
            if len(date1) > 10:
                date1 = datetime.datetime.strptime(date1, '%Y-%m-%d %H:%M:%S')
            else:
                date1 = datetime.datetime.strptime(date1, '%Y-%m-%d')
            
            if  date1 >= nextNavDate:
                str1 += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>"
                str1 += "<a class='anchor' name='" + str(nextNavDate) + "' href='#top'>top</a> | "
                # ~ prev = nextNavDate + datetime.timedelta(days=-1)
                prev = self.getPrevNavDate(nextNavDate)
                str1 += "<a href='#" + str(prev) + "'>prev</a> | "
                str1 += str(nextNavDate) + " | " 
                # ~ nextNavDate = nextNavDate + datetime.timedelta(days=1)
                nextNavDate = self.getNextNavDate(nextNavDate)
                str1 += "<a href='#" + str(nextNavDate) + "'>next</a> | "
                str1 += "<a href='#bottom'>bottom</a>"
                str1 += "</td></tr>"
            
            str1 += "<tr>"
            str1 += "<td>" + n['datum'] + "</td>" 
            str1 += "<td>" + self.formTopic(n['topic']) + "</td>"
            
            #TODO mouse hover shows text???
            str1 += "<td>"
            str1 += "<a href=" + n['url'] + " target='_blank'>"
            str1 += n['title'][0:self.cfg['timeline']['titlelen']]
            str1 += "</a>"
            str1 += "</td>"
            str1 += "</tr>"
        
        str1 += "</table>"
        #date navigation header
        str1 += "<p class='navheader'>"
        str1 += "<a href='#top'>top</a> | "
        str1 += "<a class='anchor' name='bottom' href='#bottom'>bottom</a>" 
        str1 += "</p>"
        
        #footer
        str1 += "<p class='footer'>footer</p>"
        str1 += "</body></html>"
            
        filename = self.getTlHtmlFilename()
        cupw("write timeline to=", filename)
        with open(filename, "w") as text_file:
            text_file.write(str1)

    def formTopic(self, topic):
        l1 = topic.split(self.cfg['mapper']['klammeraff'])
        str1 = ""
        if len(l1[0]) > self.cfg['timeline']['topiclen']:
            str1 += l1[0][:self.cfg['timeline']['topiclen']] + "..."
        else:
            str1 += l1[0]
            
        str1 += self.cfg['mapper']['klammeraff']

        if len(l1[1]) > self.cfg['timeline']['topiclen']:
            str1 += l1[1][:self.cfg['timeline']['topiclen']] + "..."
        else:
            str1 += l1[1]
            
        return str1
        
    
    def getCharNavList(self):
        str1 = ""
        for i in self.navlist:
            str1 += "<a href='#" + i + "'>" + i + "</a> | "
            
        return str1
    
    
    def buildTimelineHtmlTopic(self):
        '''
        build html timeline file (sort by date)
        '''
        cupw("buildTimelineHtmlTopic...")
        str1 = self.getHtmlHeader()
        str1 += "<h1>stof999 newstimeliner (sort by topic) " + self.getDateYYMMDDHHMM() + "</h1>"

        self.navlist = []
        for i in range(97,123):    
            self.navlist.append(chr(i))

        #navigation header
        str1 += "<p class='navheader'>"
        str1 += "<a class='anchor' name='top' href='#top'>top</a> | "
        str1 += self.getCharNavList()
        str1 += "<a href='#bottom'>bottom</a>"
        str1 += "</p>"


        #table header row
        str1 += "<table>"
        str1 += "<tr>"
        str1 += "<th>topic v</th>"
        str1 += "<th><a href='" + self.getTlHtmlLink() + "'>date</a></th>"
        str1 += "<th>title</th>"
        str1 += "</tr>"
        
        nextNavChar = 1
        
        for n in self.nlistsorttopic:
            
            if n['topic'] >= self.navlist[nextNavChar]:
                str1 += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>"
                str1 += "<a href='#top'>top</a> | "
                str1 += "<a href='#" + self.navlist[nextNavChar-1] + "'>prev</a> | "
                str1 += "<a class='anchor' name='" + self.navlist[nextNavChar] + "' href='#" + self.navlist[nextNavChar] + "'>" + self.navlist[nextNavChar] + "</a> | "
                nextNavChar += 1
                str1 += "<a href='#" + self.navlist[nextNavChar] + "'>next</a> | "
                str1 += "<a href='#bottom'>bottom</a>"
                str1 += "</td></tr>"
                
            
            str1 += "<tr>"
            str1 += "<td>" + self.formTopic(n['topic']) + "</td>"
            str1 += "<td>" + n['datum'] + "</td>" 
            
            #TODO mouse hover shows text???
            str1 += "<td>"
            str1 += "<a href=" + n['url'] + " target='_blank'>"
            str1 += n['title'][0:self.cfg['timeline']['titlelen']]
            str1 += "</a>"
            str1 += "</td>"
            str1 += "</tr>"
            
        str1 += "</table>"
        #date navigation header
        str1 += "<p class='navheader'>"
        str1 += "<a href='#top'>top</a> | "
        str1 += self.getCharNavList()
        str1 += "<a class='anchor' name='bottom' href='#bottom'>bottom</a>" 
        str1 += "</p>"
        
        #footer
        str1 += "<p class='footer'>footer</p>"
        str1 += "</body></html>"
            
        filename = self.getTlHtmlTopicFilename()
        cupw("write timeline to=", filename)
        with open(filename, "w") as text_file:
            text_file.write(str1)


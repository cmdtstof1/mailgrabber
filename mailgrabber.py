#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

'''
mailgrabber catches "news email" out from draft folder,
and
- adds them in "news files"
- stores the "news mail" in archive
- and produces news timelines
- "news email" will be deleted if option is set.

>>> readme.txt
'''

import sys
import json
import lib.timeliner
import lib.newsbuilder
import lib.grabber
import lib.vercoder


class App:

    def __init__(self, larg):
        self.larg = larg
        self.opt = self.setDefaultOpt() #1: default options
        self.parseArg(self.larg) #2: parse arg and overwrite options <list >dict 
        self.cfg = self.loadcfg("cfg.json")
   
   
    def loadcfg(self, filename):
        with open(filename) as json_file:
            return json.load(json_file)

    
    def setDefaultOpt(self):
        opt = {
            '--mailgrab': 0, #scrab emails from draft and process them
            '--mailserver': 'test',
            '--mailuser': 'test@localhost',
            '--mailpwd': 'testpwd',
            '--mapfile': 'data/mapfile.csv',
            '--export': 'txt',
            '--exportdir': 'data/export/',
            '--exportappend': 1,
            '--exporttimelinedir': 'data/export/time/', #exportdir for timeline
            '--exporttimelinedirhtml': 'data/export/time/', #exportdir for timeline html
            '--exporttimelinewwwbase': '', #base url for links in html
            '--exporttimelinefile': 'newsline', # sort by date
            '--exporttimelinefiletopic': 'newsline_topic', # sort by topic
            '--mailarchiv': 'data/archiv/',
            '--maildel': 0,
            '--timeliner': 0,  #creates newstimeline out of news files
            '--timelinenav': "month", #day/month
            '--makenewfromemail': 0, #makes news file NEW from stored emails (newsbuilder)
            '--encodeverkacker': 0 #tries to rescue the encoding verkack...
            }
        return opt
    
    def parseArg(self, larg): #<list >opt-dict, word-list
        larg = larg[1:] #remove 1. arg (=caller name)
        for a in larg:
            if a.startswith("--"):
                l1 = a.split("=")
                self.opt[l1[0]] = l1[1]
                
            else:
                cupw("TODO: what to do with opt=", a)
                

        
#######################################3
if __name__ == '__main__':
    
    cupw("**********mailgrabber starts...")
    larg = sys.argv
    app = App(larg)
    
    if app.opt['--mailgrab']:
        cupw("grab mails and write in news file...")
        gr = lib.grabber.Grabber(app)
        gr.main()
        
    if app.opt['--timeliner']:
        cupw("make news timeline...")
        tl = lib.timeliner.Timeliner(app)
        tl.main()
        
    if app.opt['--makenewfromemail']:
        cupw("make news file new from email...")
        nb = lib.newsbuilder.Newsbuilder(app)
        nb.buildNews()
        
    if app.opt['--encodeverkacker']:
        cupw("email vercoder...")
        vr = lib.vercoder.Vercoder(app)
        vr.main()
        
    
    cupw("**********...mailgrabber fin")
        
        
        
        
        
        
